#!/bin/bash

set -e

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

echo ""


echo -e "  ${YELLOW}Please ensure that you have correct ~/.aws/credentials${NC}"
echo -e "  ${YELLOW}With access to free tear (at least)${NC}"

echo ""

echo -ne "       ${YELLOW}Checking ansible installation${NC}"
command -v ansible-playbook 1>/dev/null && echo -e "\r  ${GREEN} OK ${NC}" || (echo -e "\r ${RED} FAIL ${NC}" && echo " No 'ansible-playbook' find in PATH." && false)

echo ""

echo -ne "       ${YELLOW}Checking terraform installation${NC}"
command -v terraform 1>/dev/null && echo -e "\r  ${GREEN} OK ${NC}"  || (echo -e "\r ${RED} FAIL ${NC}" && echo " No 'terraform' find in PATH." && false)
echo ""

echo -ne "       ${YELLOW}Checking utils: jk ${NC}"
command -v jq 1>/dev/null && echo -e "\r  ${GREEN} OK ${NC}"  || (echo -e "\r ${RED} FAIL ${NC}" && echo " No 'jq' find in PATH." && false)

echo -ne "       ${YELLOW}Checking utils: curl ${NC}"
command -v curl 1>/dev/null && echo -e "\r  ${GREEN} OK ${NC}" || (echo -e "\r ${RED} FAIL ${NC}" && echo " No 'curl' find in PATH." && false)
echo ""

echo -ne "       ${YELLOW}Checking if tfvars are present${NC}"
if [ ! -f ./terraform/terraform.tfvars ]; then
  echo -e "\r  ${YELLOW} WARNING ${NC}"
  echo ""
  echo "   ${YELLOW}Creating ./terraform/terraform.tfvars using example${NC}"
  echo "   ${YELLOW}Remember to configure them manually${NC}"
  echo ""
  cd terraform
  cp terraform.tfvars.example terraform.tfvars
  cat terraform.tfvars
  cd ..
else
  echo -e "\r  ${GREEN} OK ${NC}"
  echo -e "       ${YELLOW}Tfvars are present${NC}"
fi
echo ""
echo -ne "       ${YELLOW}Checking if hosts has been already created${NC}"
if [ ! -f ./terraform/terraform.tfstate ] || [ $(cat terraform/terraform.tfstate 2>/dev/null| grep aws | wc -l) -eq 0 ]; then
  echo -e "\r  ${YELLOW} WARNING ${NC}"
  echo "   ${YELLOW}Running terraform${NC}"
  cd terraform
  terraform init
  echo "yes" | terraform apply
  cd ..
else
  echo -e "\r  ${GREEN} OK ${NC}"
  echo -e "       ${YELLOW}Base instances has been created already${NC}"
fi
echo ""
echo -ne "       ${YELLOW}Checking play results file${NC}"
if [ ! -f ./endpoints.json ]; then
  echo -e "\r  ${YELLOW} WARNING ${NC}"
  echo -e "  ${YELLOW}Looks like endpoints has not been configured yet, run ansible${NC}"
  cd ansible
  sleep 20
  ansible-playbook -i ../terraform/terraform.tfstate site.yml
  cd ..
else
  echo -e "\r  ${GREEN} OK ${NC}"
  echo -e "       ${YELLOW}Ansible provision has been finished${NC}"
fi

echo ""
echo -e "  ${YELLOW}Provision finished, here are the web endpoints${NC}"
cat endpoints.json | jq '.[] | select(.grp=="web_pool")'
echo -e "  ${YELLOW}Checking the services${NC}"
echo -e "  ${YELLOW}Insert new data${NC}"
curl -XPOST `cat endpoints.json | jq '[.[] | select(.grp=="web_pool")]' |  jq .[].public_ip | tr -d '"' | head -n 2 | tail -n 1`
sleep 5
echo -e "  ${YELLOW}Get all data from all shards${NC}"
curl -XGET `cat endpoints.json | jq '[.[] | select(.grp=="web_pool")]' |  jq .[].public_ip | tr -d '"' | head -n 1 | tail -n 1`