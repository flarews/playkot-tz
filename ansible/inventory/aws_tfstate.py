#encoding=utf-8
import operator
import os

from collections import Mapping, Set, Sequence
from functools import reduce

from ansible.errors import AnsibleParserError
from ansible.plugins.inventory import BaseFileInventoryPlugin
from ansible.module_utils._text import to_native

string_types = (str, unicode) if str is bytes else (str, bytes)
iteritems = lambda mapping: getattr(mapping, 'iteritems', mapping.items)()


def objwalk(obj, path=(), memo=None):
    """
    Iterate through an object of any complexity.
    Shotouts to stackoverflow.
    """

    if memo is None:
        memo = set()
    iterator = None
    if isinstance(obj, Mapping):
        iterator = iteritems
    elif isinstance(obj, (Sequence, Set)) and not isinstance(obj, string_types):
        iterator = enumerate
    if iterator:
        if id(obj) not in memo:
            memo.add(id(obj))
            for path_component, value in iterator(obj):
                for result in objwalk(value, path + (path_component,), memo):
                    yield result
            memo.remove(id(obj))
    else:
        yield path, obj


def filter_items(iterable_data, boolean_func=None):
    """
    Allows to filter json-like objects of unknown complexity.
    Objwalk() through an object and returns all subitems of given item where boolean_func is True.

    Example:

    d = {1: 2, 3: [4, 5] }
    sub = filter_items(d, lambda subkey: 5 in subkey)
    assert sub == { 3: [4, 5] }
    """
    keys = []
    if not boolean_func:
        return iterable_data
    else:
        for path, value in objwalk(iterable_data):
            try:
                if boolean_func(value):
                    keys.append(path[0])
            except:
                for n in range(0, len(path)):
                    try:
                        if boolean_func(reduce(operator.getitem, path[:-n], iterable_data)):
                            keys.append(path[0])
                    except:
                        pass

        if isinstance(iterable_data, Mapping):
            return {k: iterable_data[k] for k in set(keys)}
        elif isinstance(iterable_data, (Sequence, Set)) and not isinstance(iterable_data, string_types):
            return [iterable_data[k] for k in set(keys)]


class TFState:

    """
    Reorders the tfstate data to pre2.3 dynamic inventory json.
    Supports tfstate version >= 3.
    """

    def __init__(self, json_data):
        self._raw = None
        self.serial_data = dict()
        self._meta = dict(hostvars={})
        try:
            self._raw = json_data
            assert self._raw['version'] >= 3
        except AssertionError as e:
            print("TFState file version is invalid")
            raise e

        self._get_aws_hosts()

    def _get_aws_hosts(self):
        modules = filter_items(self._raw['modules'], lambda module: module["type"] == "aws_instance")
        for module in modules:
            resources = filter_items(module['resources'], lambda resource: resource["type"] == "aws_instance")
            for res_name, attributes in resources.items():

                group_name = attributes['primary']['attributes']["tags.Group"]
                host_name = attributes['primary']['attributes']["tags.Name"]
                host_ip = attributes['primary']['attributes']["public_ip"]

                if group_name not in self.serial_data:
                    self.serial_data[group_name] = {
                        'hosts': [],
                        'vars': [],
                        'children': []
                    }

                if host_name not in self.serial_data[group_name]['hosts']:
                    self.serial_data[group_name]['hosts'].append(host_name)
                    self._meta['hostvars'][host_name] = {
                        'ansible_ssh_host': host_ip,
                        'ansible_host': host_ip,
                        'ansible_ssh_user': 'ubuntu',
                        'ansible_user': 'ubuntu'
                    }
                    # python3 allows to replace this with **unpacking in statement above
                    for k, v in attributes['primary']['attributes'].items():
                        self._meta['hostvars'][host_name][k] = v

    def serialize(self):
        serial = self.serial_data
        serial['_meta'] = self._meta
        return serial


class InventoryModule(BaseFileInventoryPlugin):
    """
    Ansible 2.4+ inventory plugin.
    Supports python 2.7/3.6

    Usage: ansible-playbook -i /path/to/terraform.tfstate <...>
    """

    NAME = "aws_tfstate"

    def __init__(self):
        super(InventoryModule, self).__init__()
        self._hosts = set()

    def verify_file(self, path):
        valid = False
        if super(InventoryModule, self).verify_file(path):
            file_name, ext = os.path.splitext(path)
            if not ext or ext == '.tfstate':
                valid = True
        return valid

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)

        try:
            data = self.loader.load_from_file(path)
        except Exception as e:
            raise AnsibleParserError(e)

        tfstate = TFState(data)
        data_from_meta = None

        for group_name, group_data in tfstate.serialize().items():

            if group_name == '_meta':
                if 'hostvars' in group_data:
                    data_from_meta = group_data['hostvars']
            else:
                self.inventory.add_group(group_name)
                for hostname in group_data['hosts']:
                    self._hosts.add(hostname)
                    self.inventory.add_host(hostname, group_name)

        for host in self._hosts:
            got = {}
            try:
                got = data_from_meta.get(host, {})
            except AttributeError as e:
                raise AnsibleError("Improperly formatted host information for %s: %s" % (host, to_native(e)))
            try:
                self.populate_host_vars([host], got)
            except:
                self._populate_host_vars([host], got)   # thats how it will work in 2.5
