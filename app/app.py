from lib.config import Config
from bson import json_util
import json
from flask import Flask, request, jsonify
from pymongo import MongoClient
from datetime import datetime
app = Flask(__name__)

cfg = Config()
client1 = MongoClient(cfg.get_db_url('replset1'))
client2 = MongoClient(cfg.get_db_url('replset2'))
db1 = client1.test_db
db2 = client2.test_db


@app.route('/', methods=['GET', 'POST'])
def root():
    if request.method == 'POST':

        post = {
            'lol': 'kek',
            'date': str(datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.000Z"))
        }

        db1.test_collection.insert_one(post)
        db2.test_collection.insert_one(post)

        return jsonify(json.loads(json_util.dumps(post)))

    elif request.method == "GET":
        out = []
        for post in db1.test_collection.find({}):
            out.append(post)
        for post in db2.test_collection.find({}):
            out.append(post)

        return jsonify(json.loads(json_util.dumps({'data': out})))
