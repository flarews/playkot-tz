__author__ = 'flare'

from configparser import ConfigParser
import os


class Config:
    def __init__(self):
        self._cfgfile = ConfigParser()
        _libdir = os.path.dirname(os.path.abspath(__file__))
        self._cfgfile.read(os.path.join(
            _libdir, 'config.ini'), encoding='utf-8')
        self._env = None

    def get(self, stage, param):
        tmp = None
        try:
            tmp = self._load_from_env(param)
        except:
            pass
        if not tmp:
            tmp=str(self._load_from_file(stage, param))
        return tmp

    def _load_from_env(self, param):
        tmp = None
        try:
            tmp = os.environ[param]
        except KeyError:
            print(os.environ)
            print('No such var found in env, %s' % param)
        return tmp

    def _load_from_file(self, stage, param):
        tmp = None
        try:
            tmp = self._cfgfile.get(stage, param, raw=True)
        except KeyError:
            print('No such var found in file, %s' % param)
        return tmp

    def get_db_url(self, stage):
        template = "mongodb://{dbhost}/?replicaSet={dbreplset}"
        return template.format(
            dbhost=self.get(stage, 'DBHOSTS'),
            dbreplset=self.get(stage, 'DBREPLSET')
            )
