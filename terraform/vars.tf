variable ssh_access_name {
  description = "Name of ssh key to create"
}

variable ssh_access_key_public {
  description = "Path to ssh public key used to create this key on AWS"
}

