variable nginx_count {
    default = "1"
    description = "Number of instances"
}

variable nginx_name {
    default = "web"
    description = "Server name"
}

variable nginx_sg_ids {
   description = "List of security groups ids"
}

variable nginx_ssh_access_name {
    description = "Ssh key name"
}