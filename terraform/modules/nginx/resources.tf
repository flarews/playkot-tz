resource "aws_instance" "nginx" {
    ami = "ami-5055cd3f"                # Ubuntu Server 16.04 LTS (HVM) SSD
    instance_type = "t2.micro"
    count = "${var.nginx_count}"
    key_name = "${var.nginx_ssh_access_name}"

    vpc_security_group_ids = [
      "${var.nginx_sg_ids}",
      "${aws_security_group.allow_nginx_sg.id}",
    ]

    tags {
        Name  = "${format("${var.nginx_name}%02d", count.index+1)}"
        Group = "${var.nginx_name}_pool"
    }

}

resource "aws_security_group" "allow_nginx_sg" {
  name        = "allow_nginx_sg"
  description = "Allow HTTP access"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
