resource "aws_key_pair" "auth" {
  key_name   = "${var.ssh_access_name}"
  public_key = "${file("${var.ssh_access_key_public}")}"
}

resource "aws_security_group" "ssh_access_allow_sg" {
  name        = "ssh_access_allow_sg"
  description = "Allow SSH access"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}