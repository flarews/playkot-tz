resource "aws_security_group" "allow_mongodb_sg" {
  name        = "allow_mongodb_sg"
  description = "Allow DB access"

  ingress {
    from_port   = 27017
    to_port     = 27019
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # TODO: make a VPC and access db via private subnet
  }
  egress {
    from_port   = 27017
    to_port     = 27019
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}