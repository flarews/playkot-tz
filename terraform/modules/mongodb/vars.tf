variable mongodb_instances {
    default = "3"
    description = "Number of instances"
}

variable mongodb_name {
    default = "db"
    description = "Server name"
}

variable mongodb_sg_id_ssh {
    description = "List of security groups ids"
}

variable mongodb_sg_id_mongodb {
    description = "List of security groups ids"
}

variable mongodb_ssh_access_name {
    description = "Ssh key name"
}