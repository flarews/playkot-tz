resource "aws_instance" "mongodb" {
    ami = "ami-5055cd3f"                # Ubuntu Server 16.04 LTS (HVM) SSD
    instance_type = "t2.micro"
    count = "${var.mongodb_instances}"
    key_name = "${var.mongodb_ssh_access_name}"

    vpc_security_group_ids = [
      "${var.mongodb_sg_id_ssh}",
      "${var.mongodb_sg_id_mongodb}"
    ]

    tags {
        Name  = "${format("${var.mongodb_name}%02d", count.index+1)}"
        Group = "${var.mongodb_name}_pool"
    }

}