provider "aws" {
    region = "eu-central-1"
}

module "ssh_access" {
    source = "modules/ssh_access"
    ssh_access_key_public = "${var.ssh_access_key_public}"
    ssh_access_name = "${var.ssh_access_name}"
}

module "mongodb_access" {
    source = "modules/mongodb_access"
}

module "nginx" {
    source = "modules/nginx"
    nginx_count = 2
    nginx_sg_ids = "${module.ssh_access.ssh_access_allow_sg_id}"
    nginx_ssh_access_name = "${var.ssh_access_name}"
}

module "mongodb_shard1" {
     source = "modules/mongodb"
     mongodb_instances = 3
     mongodb_name = "shard1"
     mongodb_ssh_access_name = "${var.ssh_access_name}"
     mongodb_sg_id_ssh = "${module.ssh_access.ssh_access_allow_sg_id}"
     mongodb_sg_id_mongodb = "${module.mongodb_access.mongodb_access_allow_mongodb_sg_id}"
}

module "mongodb_shard2" {
    source = "modules/mongodb"
    mongodb_instances = 3
    mongodb_name = "shard2"
    mongodb_ssh_access_name = "${var.ssh_access_name}"
    mongodb_sg_id_ssh = "${module.ssh_access.ssh_access_allow_sg_id}"
    mongodb_sg_id_mongodb = "${module.mongodb_access.mongodb_access_allow_mongodb_sg_id}"
}